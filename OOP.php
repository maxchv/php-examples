<?php
# Объектно ориентированно программирование

# Основы 
// Простое определение класса
class SimpleClass
{
    // объявление свойства
    public $var = 'значение по умолчанию';

    // объявление метода
    public function displayVar() {
        echo $this->var;
    }
}

// Переменная $this
class A
{
    function foo()
    {
        if (isset($this)) {
            echo '$this определена (';
            echo get_class($this);
            echo ")\n";
        } else {
            echo "\$this не определена.\n";
        }
    }
}

class B
{
    function bar()
    {
        // Замечание: следующая строка вызовет предупреждение, если включен параметр E_STRICT.
        A::foo();
    }
}

$a = new A();
$a->foo();

// Замечание: следующая строка вызовет предупреждение, если включен параметр E_STRICT.
A::foo();
$b = new B();
$b->bar();

// Замечание: следующая строка вызовет предупреждение, если включен параметр E_STRICT.
B::bar();

# Создание экземпляра класса
$instance = new SimpleClass();

// Это же можно сделать с помощью переменной:
$className = 'SimpleClass';
$instance = new $className(); // Foo()

# Константы в классах
class MyClass
{
    const CONSTANT = 'значение константы';

    function showConstant() {
        echo  self::CONSTANT . "\n";
    }
}

echo MyClass::CONSTANT . "\n";

$classname = "MyClass";
echo $classname::CONSTANT . "\n"; // начиная с версии PHP 5.3.0

$class = new MyClass();
$class->showConstant();

echo $class::CONSTANT."\n"; // начиная с версии PHP 5.3.0

//Declare a base class with a constructor and destructor. The __toString() method is useful for
//serializing the object to a string.
class SuperPerson
{
    public $Name; // Accessible to anyone
    public $PowerLevel;
    // Constructor
    public function __construct($Name, $PowerLevel = 0) {
        $this->Name = $Name;
        $this->PowerLevel = $PowerLevel;
    }
    // Destructor called when object goes out of scope
    function __destruct() {
        echo "Bye-bye";
    }
    // Convert object to string representation
    public function __toString() {
        return "Name = $this->Name, PowerLevel = $this->PowerLevel\n";
    }
}

//Extend the base class from above (SuperPerson) to create a super hero:
class SuperHero extends SuperPerson
{
    private $savedVictims = 0; // Accessible only within the class
    public function Save($victim) {
        echo "$this->Name is saving $victim.\n";
        $this->savedVictims++;
    }
    public function GetNumberOfSavedVictims() {
        return $savedVictims;
    }
}
//Declaring and using objects:
$hero = new SuperHero("Spam-Man", 3);
echo "$hero"; // Prints Name = Spam-Man, PowerLevel = 3
$hero->Save("Laura Jones");