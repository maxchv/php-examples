<?php
    header("Content-type: text/html; charset=utf8");
    /*
    * Настройки PHP.INI
    * file_uploads = "1"
    * upload_max_filesize = "2M"
    * post_max_size = "8M"
    * max_file_uploads = 20    
    */
?>
<h1>Пример отправки данных формы через ajax с получением ответа</h1>
<form id="upload" method="post" action="#" enctype="multipart/form-data">
    <!--<input type="hidden" name="MAX_FILE_SIZE" value="4096" />-->
    <input type="text" name="text">
    <input type="file" name="file">
    <input type="submit">
</form>

<progress value="0" min="0" max="100" width="350"></progress>
<br>

    <div id="result"></div>

<script src="http://code.jquery.com/jquery-2.1.4.js"></script>
<script>
    $("#upload").submit(function(event){
        event.preventDefault();
        event.stopPropagation();
        var formData = new FormData($("form")[0]);
        $.ajax({
            url: 'upload.php',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    myXhr.upload.addEventListener('progress',
                                                    progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            //Ajax events
            //beforeSend: beforeSendHandler,
            success: function(data) {
              $("#result").html(data);  
            },
            //error: errorHandler,
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });
        
        function progressHandlingFunction(e){
            if(e.lengthComputable){
                $('progress').attr({value:e.loaded, max:e.total});
            }
        }
        
        console.log("Submit");
    });
</script>

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

