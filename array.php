﻿<?php
#Работа с массивами

include_once 'utils.php';

//Нумерованный массив
$streets = array("Ленина", "Гоголя", "Телевизионная"); //отсчет с нуля
show($streets);

$presidents = array(22 => "Билл Клинтон", "Джордж Уокер Буш", "Барак Обама"); //отсчет с 42
show($presidents);
// Ассоциированный массива
$pres = array("Bill" => "Билл Клинтон",
              "Bush" => "Джордж Уокер Буш",
	      "Barak" => "Барак Обама");
				 
$years = range(2001, 2010); //тут 10 элементов
show($years);

// PHP version > 5.4
$like_js_array = [ 'one', 'two', 'tree'];
show($like_js_array);

//Создает новый массив, используя один массив в качестве ключей,
// а другой в качестве соответствующих значений

$a = array('green', 'red', 'yellow');
$b = array('avocado', 'apple', 'banana');
$c = array_combine($a, $b);
show($c);

// Вложенные массивы
$multiple_arr = array 
(
    'inner_one' => array( 1,2,3),
    'inner_two' => array(4,3,2),
);
show($multiple_arr);

// Извлечение данных из массива в переменные - только для нумерованных массивов
// начинающихся с 0
list($lenina, $gogola, $tv) = $streets;
show($lenina, $gogola, $tv);

//Работа с массивами
current($years);  //pos($years) возвращает текущее значение элемента масива, не перемещая указатель
next($years);// перемещает указатель вперед на один элемент
prev($years);// перемещает указательназад на один элемент
end($years);// перемещает указатель в конец массива
key($years); // ключ для текущего элемента массива
reset($years);// перемещает указатель в начало массива


// Перебор элементов массива
while(list($key, $value) = each($years)) {
    show($key, $value);
}

foreach($pres as $en => $ru) //после прохода указатель возвращаетс¤ в начало
{
   show($en, $ru);
}

$name = "show";

//пример прохода по массиву
foreach($_SERVER as $key => $value) //информаци¤ о сервере
{
    //show($key, $value);
}

//Разделение строки
$str = "это: новый: дом";
$expl = explode(":", $str);
show($expl);

//объединение строки
$joined = implode(";", $expl); // join(";", $expl);
show($joined);

// Извлечение из массива данных
$colors = array("red" => "красный", "blue" => "синий");
extract($colors);
show($red, $blue);

// Противоположная операция - объединение данных в массив
$color1 = "красный";
$color2 = "синий";
$a = "фиолет";
$b = "оранж";
$arrayIn = array("a", "b");
$arrayOut = compact("color1", "color2", $arrayIn);
show($arrayIn, $arrayOut);

// Получение под массива
$colors = array("крас", "зел", "син", "роз");
$sub = array_slice($colors, 1, 2); //1элемен с которого начинаетс¤ разбиение, 2 - количество элементов
show($colors, $sub);

// удаление элементов массива
unset($colors[0]);
show($colors);
array_splice($colors, 1, 2); // со второго удалить два элемента
show($colors);

// Для каждого элемента массива вызвать функцию и возвратить результат в виде массива
array_map("show", $arrayIn); 

// Применяет заданную пользователем функцию к каждому элементу массива
array_walk($arrayIn, function(&$item, $key){
   $item = $item . $key;
});
show($arrayIn);

// добавляет новый элемент массива в конец
array_push($colors, "красный");
// array_pop($colors) - возвращает с конца и удаляет

// добавляет новый элемент массива в начало
array_unshift($colors, "красный");
// array_shift($colors) - возвращает с начала и удаляет
show($colors);

// возвращает массив из уникальных элементов
show(array_unique($colors));

// Объединение массивов
//$big = array_merge($массив1, $массив2, ...);
//
//суммирование элементов массива

////$sum = array_sum($им¤_массива);
//

//Перестановка местами ключей и значений
$l['r'] = 'red';
$l['b'] = 'blue';
show($l);
////перестановка
$pere = array_flip($l);
////результат обработки
show($pere);

// Проверка наличия ключа
if(array_key_exists("blue", $pere)) {
    echo "exist<br/>";
}

// проверка наличия значения
if(in_array("крас", $colors))
{
    echo "<br>Красный</br>";
}

// поиск позиции в массиве
$idx = array_search(2001, $years);

// Фильтр данных массива
$res = array_filter($years, function($year){
    return $year > 2005;
});
show($res);

// Реверс массива
show(array_reverse($years));

// разброс данных массива
shuffle($years);
show($years);

// Сортировка нумерованного массива
sort($years);
show($years);

// сортировка с использованием пользовательской функции сортировки
$files = ["doc1.txt", "doc5.txt", "doc2.txt", "doc10.txt", "doc6.txt"];
usort($files, function($a, $b){
    return strnatcmp($a, $b);
});
show($files);

// Сортировка ассоциированного массива пользовательской функцией
function my_sort($a,$b)
{
    if ($a==$b) 
        return 0;
    return ($a<$b)?-1:1;
}

$arr=array("a"=>4,"b"=>2,"c"=>8,"d"=>"6");
uasort($arr,"my_sort");

//Сортирует массив по ключам, сохраняя отношения между ключами и значениями. 
//Эта функция полезна, в основном, для работы с ассоциативными массивами.

$fruits = array("d"=>"lemon", "a"=>"orange", "b"=>"banana", "c"=>"apple");
ksort($fruits);
show($fruits);