<?php
# Подключание к базе данных через MySQLi
# http://www.w3schools.com/php/php_mysql_connect.asp
    namespace db\MySQLi;
    
    include '../mysql_connection.inc.php';

    // Create connection
    $conn = new \mysqli($servername, $username, $password);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
    echo "Connected successfully";
    // The connection will be closed automatically when the script ends. 
    // To close the connection before, use the following:
    $conn->close();
    
