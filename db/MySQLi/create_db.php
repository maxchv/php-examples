<?php
# Создание базы данных с помощью MySQLi
    namespace db\MySQLi;
    include '../mysql_connection.inc.php';
    
    // Create connection    
    $conn = new \mysqli($servername, $username, $password);
    // Check connection
    
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 

    // Create database
    $sql = "CREATE DATABASE myDBMySQLi";
    if ($conn->query($sql) === TRUE) {
        echo "Database created successfully";
    } else {
        echo "Error creating database: " . $conn->error;
    }

    $conn->close();

