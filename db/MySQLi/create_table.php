<?php
# Создание таблиц с помощью MySQLi
# http://www.w3schools.com/php/php_mysql_create_table.asp
    namespace db\MySQLi;
    include '../mysql_connection.inc.php';
    
    $dbname = "myDBMySQLi";
    $conn = new \mysqli($servername, $username, $password, $dbname);
    if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    // sql to create table
    $sql = "CREATE TABLE MyGuests (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                firstname VARCHAR(30) NOT NULL,
                lastname VARCHAR(30) NOT NULL,
                email VARCHAR(50),
                reg_date TIMESTAMP
            )";
    
    if($conn->query($sql) === TRUE) {
        echo "Table MyGuests created successfuly";
    } else {
        echo "Error creation table: " . $conn->error;
    }
    
    $conn->close();
