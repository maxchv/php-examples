<?php
# Вставка данных в таблицы с помощью MySQLi
# http://www.w3schools.com/php/php_mysql_insert.asp
# http://www.w3schools.com/php/php_mysql_insert_lastid.asp
    namespace db\MySQLi;
    include '../mysql_connection.inc.php';
    
    $dbname = "myDBMySQLi";
    
    $conn = new \mysqli($servername, $username, $password, $dbname);
    if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "insert into MyGuests (firstname, lastname, email)"
            . "values ('John', 'Doe', 'john@example.com')";
    if($conn->query($sql) === TRUE) {
        $last_id = $conn->insert_id;
        echo "New record created successfully. Last inserted ID is: $last_id";
    } else {
        echo "Error: $sql <br> {$conn->error}";
    }
    
    $conn->close();
    
    

