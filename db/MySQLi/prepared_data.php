<?php
# Подготовленные выражения в MySQLi
# http://www.w3schools.com/php/php_mysql_prepared_statements.asp
    namespace db\MySQLi;
    include '../mysql_connection.inc.php';
    
    $dbname = "myDBMySQLi";
    
    $conn = new \mysqli($servername, $username, $password, $dbname);
    if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    // prepare and bind
    $stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) "
                         . "VALUES (?, ?, ?)");
    //The argument may be one of four types:
    //  i - integer
    //  d - double
    //  s - string
    //  b - BLOB
    $stmt->bind_param("sss", $firstname, $lastname, $email);
    
    // set parameters and execute
    $firstname = "John";
    $lastname = "Doe";
    $email = "john@example.com";
    $stmt->execute();

    $firstname = "Mary";
    $lastname = "Moe";
    $email = "mary@example.com";
    $stmt->execute();

    $firstname = "Julie";
    $lastname = "Dooley";
    $email = "julie@example.com";
    $stmt->execute();

    echo "New records created successfully";

    $stmt->close();
    $conn->close();
