<?php
# Выборка данных в MySQLi
# http://www.w3schools.com/php/php_mysql_prepared_statements.asp
    namespace db\MySQLi;
    include '../mysql_connection.inc.php';
    $dbname = "myDBMySQLi";
    
    // Создаем соединение
    $conn = new \mysqli($servername, $username, $password, $dbname);
    // Проверяем соединение
    if($conn->error) {
        die("Connection failed: " . $conn->error);
    }
    
    $sql = "select id, firstname, lastname from MyGuests";
    $stmt = $conn->prepare($sql);
    //$stmt->bind_param("s", $var)
    $result = $stmt->execute();
    $stmt->bind_result($result);    
    $stmt->fetch();

    print_r($result);
    
    $stmt->close();
    $conn->close();