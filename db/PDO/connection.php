<?php
# Подключание к базе данных через PDO
# http://www.w3schools.com/php/php_mysql_connect.asp
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=myDB", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        echo "Connected successfully";         
    }
    catch(\PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }
    
    // The connection will be closed automatically when the script ends. 
    // To close the connection before, use the following:
    $conn = null;

