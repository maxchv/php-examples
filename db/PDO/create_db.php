<?php
# Создание базы данных с помощью PDO
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=myDB", $username, $password);
        // генерировать исключения при ошибках
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $sql = "create database myDBPDO";
        // если не нужен результат - испоьлзуем метод exec()
        $conn->exec($sql);
        echo "Database created successfuly<br>";
    } catch (\PDOException $ex) {
        echo $sql . "<br>" . $ex->getMessage();
    }
    
    $conn = null;

