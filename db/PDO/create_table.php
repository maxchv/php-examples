<?php
# Создание таблиц с помощью PDO 
# http://www.w3schools.com/php/php_mysql_create_table.asp
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    $dbname = "myDBPDO";
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
        // sql to create table
        $sql = "CREATE TABLE MyGuests (
                    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
                    firstname VARCHAR(30) NOT NULL,
                    lastname VARCHAR(30) NOT NULL,
                    email VARCHAR(50),
                    reg_date TIMESTAMP
                )";
        $conn->exec($sql);
        echo "Table MyGuests created successfuly";
    } catch (\PDOException $ex) {
        echo "Error creation table: " . $ex->getMessage();
    }   
    $conn = null;
    