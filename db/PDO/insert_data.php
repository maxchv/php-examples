<?php
# Добавление данных с помощью PDO 
# http://www.w3schools.com/php/php_mysql_insert.asp
# http://www.w3schools.com/php/php_mysql_insert_lastid.asp
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    $dbname = "myDBPDO";
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
        // sql to create table
         $sql = "insert into MyGuests (firstname, lastname, email)"
            . "values ('John', 'Doe', 'john@example.com')";
        $conn->exec($sql);
        
        $last_id = $conn->lastInsertId();
        echo "New record created successfully. Last inserted ID is: " . $last_id;
    } catch (\PDOException $ex) {
        echo "Error : " . $ex->getMessage();
    }   
    $conn = null;


