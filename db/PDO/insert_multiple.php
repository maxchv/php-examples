<?php
# Множественная вставка данных в таблицы с помощью PDO 
# http://www.w3schools.com/php/php_mysql_insert_multiple.asp
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    $dbname = "myDBPDO";
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
        
        // begin the transaction
        $conn->beginTransaction();
        
        // our SQL statememtns
        $conn->exec("INSERT INTO MyGuests (firstname, lastname, email) 
                     VALUES ('John', 'Doe', 'john@example.com')");
        $conn->exec("INSERT INTO MyGuests (firstname, lastname, email) 
                     VALUES ('Mary', 'Moe', 'mary@example.com')");
        $conn->exec("INSERT INTO MyGuests (firstname, lastname, email) 
                     VALUES ('Julie', 'Dooley', 'julie@example.com')");

        // commit the transaction
        $conn->commit();
        
        echo "New records created successfully";
    } catch (\PDOException $ex) {
        // roll back the transaction if something failed
        $conn->rollback();
        echo "Error : " . $ex->getMessage();
    }   
    $conn = null;

