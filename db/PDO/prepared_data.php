<?php
# Подготовленные выражения в PDO
# http://www.w3schools.com/php/php_mysql_prepared_statements.asp
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    $dbname = "myDBPDO";
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
        
        // prepare sql and bind parameters
        $stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) 
                                VALUES (:firstname, :lastname, :email)");
        $stmt->bindParam(':firstname', $firstname);
        $stmt->bindParam(':lastname', $lastname);
        $stmt->bindParam(':email', $email);

        // insert a row
        $firstname = "John";
        $lastname = "Doe";
        $email = "john@example.com";
        $stmt->execute();

        // insert another row
        $firstname = "Mary";
        $lastname = "Moe";
        $email = "mary@example.com";
        $stmt->execute();

        // insert another row
        $firstname = "Julie";
        $lastname = "Dooley";
        $email = "julie@example.com";
        $stmt->execute();
        
        echo "New records created successfully";
    } catch (\PDOException $ex) {
        // roll back the transaction if something failed
        $conn->rollback();
        echo "Error : " . $ex->getMessage();
    }   
    $conn = null;
