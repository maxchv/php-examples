<?php
# Выборка данных в PDO
# http://www.w3schools.com/php/php_mysql_prepared_statements.asp
# http://php-zametki.ru/php-prodvinutym/57-pdo-konstanty-vyborki-dannyx.html
    namespace db\PDO;
    include '../mysql_connection.inc.php';
    $dbname = "myDBPDO";
    
    class Data 
    {
        private $arr = array();        
        
        public function __set($name, $value) {
            $this->arr[$name] = $value;
        }
        public function __get($name) {
            return $this->arr[$name];
        }
        
        public function __isset($name) {
            return isset($this->arr[$name]);
        }
        
        public function __unset($name) {
            unset($this->arr[$name]);
        }
        
        public function __toString() {
            return html_build_query($this->arr, "; ");
        }
    }
    
    try {
        $conn = new \PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
        $stmt = $conn->prepare("SELECT id, firstname, lastname FROM MyGuests"); 
        $stmt->execute();

        // set the resulting array to associative
        //$result = $stmt->setFetchMode(\PDO::FETCH_ASSOC); 
        /*
        foreach($stmt->fetchAll() as $k=>$v) { 
            echo $k, " => ", $v, "<br/>";
        }*/
 
        // «SomeClass»  строка-имя класса, чьи объекты будут создаваться: 
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'db\PDO\Data');
        echo "<pre>";
        print_r( $stmt->fetchAll() );
        echo "</pre>";
        
    } catch (\PDOException $ex) {
        echo "Error : " . $ex->getMessage();
    }
    $conn = null;
