<!doctype html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8">
    </head>
    <body>
        <article>
            <header>
                <h2>Create new project</h2>
            </header>
            <form method="post" action="create_project.php" enctype="multipart/form-data">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" value="Название проекта"/>
                <br>
                <label for="descr">Description</label>
                <input type="text" name="descr" id="descr" value="Описание проекта" />
                <br>
                <label for="start">Start</label>
                <input type="date" name="start" id="start" value="10/07/15" />
                <br>
                <label for="end">End</label>
                <input type="date" name="end" id="end" value="20/07/15" />
                <br>
                <label for="image">Image</label>
                <input type="file" name="image" id="image" />
                <br>
                <input type="submit">
            </form>
        </article>
    </body>
</html>

