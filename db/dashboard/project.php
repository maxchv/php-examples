<?php

    class Project
    {
        private $data = array();
        private $conn;
        
        public function __construct() {            
            $this->connect_db();                    
        }
        
        public function get_request() {            
            foreach ($_REQUEST as $key => $value) {
                $this->data[$key] = filter_input(INPUT_POST, $key);
            }
            if(!empty($_FILES['image'])) {
                $this->data['image'] = $_FILES['image']['name'];
            } else {
                $this->data['image'] = NULL;
            }
        }

        public function __set($name, $value) {
            $this->data[$name] = $value;
        }
        
        public function __get($name) {
            if(array_key_exists($name, $this->data)) {
                return $this->data[$name];
            } else {
                throw new Exception("Key $name is not presnnt");
            }                
        }
        
        public function __isset($name) {
            return isset($this->data[$name]);
        }

        public function  insert_data() {
            try{
                $sql = "insert into porjects values
                ( 
                    null, 
                    '{$this->data['title']}',
                    '{$this->data['descr']}',
                    '{$this->data['start']}',
                    '{$this->data['end']}',
                    '{$this->data['image']}'
                );";
                echo $sql;
                $this->conn->exec($sql);
                $last_id = $this->conn->lastInsertId();
                echo $last_id;
            } catch (PDOException $ex) {
                echo $ex->getMessage();
            }
        }
        
        private function update_by_id($id) {
            $sql = "update porjects set title='Проект №1' where id=2;";
            // TODO
        }
        
        public function get_by_id($id) {
            $sql = "select * from porjects where id=?";
            $smtp = $this->conn->prepare($sql);
            $smtp->execute(array($id));
            $smtp->setFetchMode(PDO::FETCH_ASSOC);
            $all = $smtp->fetchAll();
            if(!empty($all)){
                //print_r($all);
                $this->data = $all[0];
            }
        }

        private function connect_db() {
            try {
                $this->conn = new PDO("mysql:host=localhost;dbname=dashboard", "root", "root");                
                $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                echo "Connected successfully";
            } catch (PDOException $ex) {
                echo "Problem with connection: {$ex->getMessage()}";
            }
        }
        
        public function __destruct() {
            if(!empty($this->conn)){
                $this->disconnect();
            }
        }
        
        private function  disconnect() {
            $this->conn = null;
        }

        public function __toString() {
            return json_encode($this->data);
        }
    }

