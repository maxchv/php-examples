<?php
# Добавить запись в базу данных dBase
namespace db\dbase;
require_once 'config.inc.php';

// открыть БД в режиме чтения и записи
$db = dbase_open($dbname, 2);

if ($db) {
  dbase_add_record($db, array(
      date('Ymd'), 
      'Maxim Topolov', 
      '23', 
      'max@example.com',
      'T'));   
  dbase_close($db);
} else {
    
}