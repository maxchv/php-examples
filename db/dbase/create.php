<?php
# Создание базы данных dBase
namespace db\dbase;
require_once 'config.ini.php';

// база данных "definition"
$def = array(
  array("date",     "D"),
  array("name",     "C",  50),
  array("age",      "N",   3, 0),
  array("email",    "C", 128),
  array("ismember", "L")
);

// создаем
// Для работы с dBase в Window$ необходимо скачать бибилеотеку по адресу
// http://windows.php.net/downloads/pecl/releases/dbase/5.1.0/
// http://pecl.php.net/package/dbase/5.1.0/windows
// после - перезапустить веб-сервер и добавить в конфиг php.ini строку
// extension=php_dbase.dll
$dbname = 'tmp/test.dbf';
if (!\dbase_create($dbname, $def)) {
    echo "Ошибка, не получается создать базу данных\n";
} else {
    echo "База данных была успешно создана и записана в файл " . $dbname ;
}

