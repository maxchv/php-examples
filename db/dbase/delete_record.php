<?php
# Удаляет запись из б/д dBase
namespace db\dbase;
require_once 'config.inc.php';

$db = \dbase_open($dbname, 2);

$nr = \dbase_numrecords($db);
if($nr) { // удаляем первую запись
    \dbase_delete_record($db, 1);
    \dbase_pack ($db);//Фиксирует удаление из базы данных
    echo "Запись №1 удалена";
}