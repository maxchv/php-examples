<?php
# Считывание всех записей базы данных dBase
namespace db\dbase;
require_once 'config.inc.php';

$db = \dbase_open($dbname, 0);

if ($db) {
  $record_numbers = \dbase_numrecords($db); // получение количества записей
  echo '<pre>';
  for ($i = 1; $i <= $record_numbers; $i++) {   
      $record = \dbase_get_record($db, $i); // получает данные в виде нумерованного массива
      $assrec = \dbase_get_record_with_names($db, $i); // получает в виде ассоциированного массива
      print_r($record);
      print_r($assrec);
  }
  echo '</pre>';
}