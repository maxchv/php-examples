<?php
# Считывание информации о полях базы данных dBase
namespace db\dbase;
require_once 'config.inc.php';

// Открываем файл БД
$dbh = dbase_open($dbname, 0)
  or die("Ошибка! Не получается открыть файл '$db_path'.");

// Получаем информацию о столбцах
$column_info = dbase_get_header_info($dbh);

// Отображение информации
echo "<pre>";
print_r($column_info);
echo "</pre>";

