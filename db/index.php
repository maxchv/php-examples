<?php
# Работа с базой данных в mysqli и PDO
namespace db;
ini_set('display_errors', '1');
header('Content-type: text/html; charset=utf-8');
echo "<ol>";
$files = array(
    /* glob("*.php") */    
    'MySQLi\connection.php',
    'PDO\connection.php',
    'MySQLi\create_db.php',
    'PDO\create_db.php',
    'MySQLi\create_table.php',
    'PDO\create_table.php',
    'MySQLi\insert_data.php',
    'PDO\insert_data.php',
    'MySQLi\insert_multiple.php',
    'PDO\insert_multiple.php',
    'MySQLi\SQLInjection.php',
    'MySQLi\prepared_data.php',
    'PDO\prepared_data.php',
    'MySQLi\select_data.php',
    'PDO\select_data.php',
    'dbase\create.php',
    'dbase\open.php',
    'dbase\header_info.php',
    'dbase\add_record.php',
    'dbase\get_records.php',
    'dbase\delete_record.php',
);
foreach ($files as $filename) {
    if (is_file($filename)) {
        $content = file($filename);
        $filepath = __NAMESPACE__ . "\\" . $filename;
        echo "<li>";
        echo substr($content[1], 1);
        echo "<ul><li>";
        echo "<a href='../showContent.php?file=" . $filepath . "'>Код</a>";
        echo "</li><li>";
        echo "<a href='" . str_replace("\\", "/", $filename) . "'>Результат</a>";
        echo "</li></ul>";
    }
}
echo "</ol>";


