<?php
#Работа с файлами
//вставить файл, если он есть
if (file_exists('footer.html')) {
    include ('footer.html');
}

if (is_file('tmp/bbb.txt')) {
    echo 'Можете не сомневаться, bbb.txt - это файл<br>';
}

if (is_dir('./tmp')) {
    echo 'Действительно, /tmp - это каталог<br>';
}

if (is_writable('tmp/db.dbf')) {
    echo 'В db.dbf писать можно<br>';
}

if (is_readable('tmp/db.dbf')) {
    echo 'db.dbf читать можно<br>';
}

if (is_executable('tmp/db.dbf')) {
    echo 'db.dbf можно запускать<br>';
}

// Информация о файле
$file = __FILE__;
echo 'Изменен: ' . date('d.m.Y H:i:s', filemtime($file)) . '<br>';
echo 'Последнее обращение: ' . date('d.m.Y H:i:s', fileatime($file)) . '<br>';
//размер файла
$filesize = filesize($file);
echo "Размер файла: $filesize байт.<br>";

//тип файла
$filetype = filetype($file);
echo "Тип файла: $filetype<br>";

//путь до каталога, им¤ и расширение файла
$pathinfo = pathinfo($file);
echo '<pre>';
var_dump($pathinfo); //массив
echo '</pre>';

chmod('tmp/bbb.txt', 0755);

touch('tmp/empty'); //создает пустой файл с заданным именем
//количество строк в файле
$kol_vo = count(file('tmp/test.txt'));
echo 'Количество строк в файле: ' . $kol_vo . '<br>';

# Работаем потоком с файлом
//r только чтение (выводитс¤ предупреждение)
//r+ чтение и запись (выводитс¤ предупреждение)
//w только запись (создаЄтс¤ новый файл, старый перезаписываетс¤)
//w+ чтение и запись (создаЄтс¤ новый файл, старый перезаписываетс¤)
//a добавление данных в конец файла (создаЄтс¤ новый файл)
//a+ чтение и добавление данных в конец файла (создаЄтс¤ новый файл)
//открываю файл и записываю в него
$nomer = fopen('tmp/nomer.txt', 'w+');
fwrite($nomer, 1 + "$nomer");
fclose($nomer);

//читаю весь файл построчно
$f = fopen('tmp/test.txt', 'r');
while (!feof($f)) {
    $line = fgets($f);
    echo $line . '<br>';
}
fclose($f);

//читаю первую строку из файла
$file = fopen('tmp/test.txt', 'r');
$line = fgets($file);
//читаю первые 5 символов из файла
$line = fgets($file, 5);
fclose($file);

# Прямая работа с файлами
// считываем весь файл в буффер вывода
readfile('tmp/test.txt');

//файл одной строкой
$content = file_get_contents('tmp/test.txt', 1);
// прямой вывод
file_put_contents('tmp/test.txt', 'Новое содержимое', FILE_APPEND);

//читаю нужную строку из файла в массив
$f = file('tmp/test.txt');
$stroka = $f[1];
echo $stroka;

//удал¤ю файл
unlink('tmp/empty');
// переименование файла
touch('tmp/tmp.txt');
rename('tmp/tmp.txt', 'tmp/temp.txt');
//копируем файл (что, куда)
if (copy('tmp/test.txt', 'tmp/test.txt~')) {
    echo "Файл был скопирован<br>";
}

//    foreach(scandir("./") as $filename) {
//        echo $filename;
//    }

// Упраженение создание и чтение журнала посещений страниц
?>