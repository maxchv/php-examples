<?php
# Применение фильтров http://www.w3schools.com/php/php_filter.asp

//Validating data = Determine if the data is in proper form.
//Sanitizing data = Remove any illegal character from the data.

#Sanitize a String
// The following example uses the filter_var() function to remove all HTML tags from a string:
$str = "<h1>Hello World!</h1>";
$newstr = filter_var($str, FILTER_SANITIZE_STRING); // Hello World!
echo $newstr;

#Validate an Integer
//The following example uses the filter_var() function to check if the variable $int is an integer. 
$int = 100;
if (filter_var($int, FILTER_VALIDATE_INT)) {
    echo("Integer is valid");
} else {
    echo("Integer is not valid");
}
//filter_var() and Problem With 0
$int = 0;
if (filter_var($int, FILTER_VALIDATE_INT) === 0 || !filter_var($int, FILTER_VALIDATE_INT) === false) {
    echo("Integer is valid");
} else {
    echo("Integer is not valid");
}

// http://php.net/manual/ru/filter.examples.validation.php
//Пример #1 Валидация e-mail адреса, используя функцию filter_var()
$email_a = 'joe@example.com';
$email_b = 'bogus';

if (filter_var($email_a, FILTER_VALIDATE_EMAIL)) {
    echo "E-mail ($email_a) указан верно.";
}
if (filter_var($email_b, FILTER_VALIDATE_EMAIL)) {
    echo "E-mail ($email_b) указан верно.";
}

//Пример #2 Валидация IP адреса, используя функцию filter_var()
$ip_a = '127.0.0.1';
$ip_b = '42.42';

if (filter_var($ip_a, FILTER_VALIDATE_IP)) {
    echo "Адрес (ip_a) указан верно.";
}
if (filter_var($ip_b, FILTER_VALIDATE_IP)) {
    echo "Адрес (ip_b) указан верно.";
}

//Пример #3 Дополнительные параметры функции filter_var()
$int_a = '1';
$int_b = '-1';
$int_c = '4';
$options = array(
    'options' => array(
                      'min_range' => 0,
                      'max_range' => 3,
                      )
);
if (filter_var($int_a, FILTER_VALIDATE_INT, $options) !== FALSE) {
    echo "Число (int_a) является верным (от 0 до 3).\n";
}
if (filter_var($int_b, FILTER_VALIDATE_INT, $options) !== FALSE) {
    echo "Число (int_b) является верным (от 0 до 3).\n";
}
if (filter_var($int_c, FILTER_VALIDATE_INT, $options) !== FALSE) {
    echo "Число (int_c) является верным (от 0 до 3).\n";
}

$options['options']['default'] = 1;
if (($int_c = filter_var($int_c, FILTER_VALIDATE_INT, $options)) !== FALSE) {
    echo "Число (int_c) является верным (от 0 и 3) и равно $int_c.";
}

// нормализация и валидация http://php.net/manual/ru/filter.examples.sanitization.php
$a = 'joe@example.org';
$b = 'bogus - at - example dot org';
$c = '(bogus@example.org)';

$sanitized_a = filter_var($a, FILTER_SANITIZE_EMAIL);
if (filter_var($sanitized_a, FILTER_VALIDATE_EMAIL)) {
    echo "Нормализованный e-mail (a) является верным.\n";
}

$sanitized_b = filter_var($b, FILTER_SANITIZE_EMAIL);
if (filter_var($sanitized_b, FILTER_VALIDATE_EMAIL)) {
    echo "Нормализованный e-mail (b) является верным.";
} else {
    echo "Нормализованный e-mail (b) является неверным.\n";
}

$sanitized_c = filter_var($c, FILTER_SANITIZE_EMAIL);
if (filter_var($sanitized_c, FILTER_VALIDATE_EMAIL)) {
    echo "Нормализованный e-mail (c) является верным.\n";
    echo "До нормализации: $c\n";
    echo "После нормализации: $sanitized_c\n";    
}