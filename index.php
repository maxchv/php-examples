<!doctype html>
<?php
ini_set('display_errors', '1');
header('Content-type: text/html; charset=utf-8');
?>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <header>
            <nav>
                    
            </nav>
        </header>
        <main>
            <article>
                <header>
                    <h2>Уроки Шага</h2>
                </header>
                <div>
                    <a href="lessons/index.php">Все уроки</a>
                </div>
            </article>
            <article>
                <header>
                    <h2>Примеры</h2>
                </header>
                <?php
                echo "<ol>";
                $files = array(
                    /* glob("*.php") */
                    'files.php',
                    'userInfo.php',
                    'OOP.php',
                    'filters.php',
                    'db/index.php',
                    'array.php'
                );
                foreach ($files as $filename) {
                    if (is_file($filename)) {
                        $content = file($filename);
                        echo "<li>";
                        echo substr($content[1], 1);
                        echo "<ul><li>";
                        echo "<a href='showContent.php?file=" . $filename . "'>Код</a>";
                        echo "</li><li>";
                        echo "<a href='" . $filename . "'>Результат</a>";
                        echo "</li></ul>";
                    }
                }
                echo "</ol>";
                ?>
            </article>
        </main>
        <footer>

        </footer>
    </body>
</html>