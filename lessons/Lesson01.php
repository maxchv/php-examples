<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>First Lesson PHP</title>
</head>
<body>
    <h1>First Lesson PHP</h1>
    <!--    Полные (стандартные) теги-->
    <?php 
        #comments
        // comments
        /* comments */
        echo "hello, world<br/>"; 
        print "PHP version: ".phpversion();
        echo "<br>";
        $name = "Max";
    ?>
    <!--    Короткие теги (включаются через опцию short_open_tag = On в php.ini-->
    <?
        echo "Hello, ".$name;
    ?>
    <!--    Теги в стиле ASP (включаются через опцию asp_tags = On в php.ini-->
    <%
       echo "<br>ASP Style";
    %>
    <!--    Устаревший синтаксис включения кода-->
    <script language="php">
        echo "<br>test<br>";
    </script>
    <?
        // Текущая дата
        /* Комментарии как в С */
        // Имя переменной начинается со знака $:
        $dat = date("d.m.y");
        // PHP - язык с динамической типизацией
        // Текущее время
        $tm = date("h:i:s");
        // Форматирование как в С (\n, \t, \\, \$, \" и т.д.
        echo "Сегодня: $dat<br />";
        echo "Сейчас: $tm";
    ?>
    <?
        // Если строка определенна в двойных ковычках, то переменная внутри нее обрабатывается, если в одинарных - то нет
        $beer='Heineken';
        echo "<br>$beer's taste is great<br />"; // work
        //echo "He drank some $beers"; // not work
        echo "He drank some ${beer}s<br>"; //work
        echo "He drank some {$beer}s<br>"; // work
    ?>
    <?php
        $str = <<<EOD
            Пример строки,
            охватывающей несколько строк
            с использованием heredoc синтаксиса <br>
EOD;
        echo "$str";
        // Получение первого символа строки
        echo "$beer[0]";
    ?>
    <h2>Преобразование типов</h2>
    <?php
        $numvar = 5;
        $strvar = "5";

        // Получение типа данных
        echo (gettype($strvar)); // string
        echo "<br />";
        echo (gettype($numvar)); // integer

        // Проверка типов (например в операторе if
        // is_integer($numvar) 
        // is_double($numvar)
        // is_string($numvar)
        // is_array($numvar)
        // is_object($numvar)
        // is_boolean($numvar)

        // Правила преобразования
        // 1. Если строка начинается с цифры, то при выполнении арифметической операции она будет преобразована в цифру, иначе - возвратит 0
        $str = "72Sgh_8";        
        $res = 5 + $str;        
        echo "<br> 5 + $str = $res";   // 77

        $str2 = "Sgh_8";
        $res2 = 5 + $str2;
        echo "<br> 5 + $str2 = $res2"; // 5
        // 2. Строка преобразуется в число с плавающей точкой только в том случае, если в строке нет буквенных символов
        $str3 = "2.8";
        $res3 = 5 + $str3;
        echo "<br> 5 + $str3 = $res3"; // 7.8

        // 3. При преобразовании в логический тип следующие значения рассматриваются как FALSE: FALSE, 0, 0.0, "0", массив с нулевыми элементами, объект с нулевыми переменными-свойствами, NULL
        // Все остальное - TRUE

        // 4. Явное преобразование типов с применением операторов (casting):
        // (int) or (integer)
        // (real), (double) or (float)
        // (bool) or (boolean)
        // (string)
        // (array)
        // (object)
        $a = 99.2;
        echo "<br>gettype($a):".gettype($a);
        echo "<br>gettype((int)$a):".gettype((int)$a);
        echo "<br>gettype((boolean)$a):".gettype((boolean)$a);
        echo "<br>gettype((string)$a):".gettype((string)$a);

        // Также можно использовать функцию settype()
        $var = "5";
        settype($var,'integer');
        echo "<br>gettype($var):".gettype($var); // integer
    ?>
    <h2>Константы</h2>
    <?php
        // для объявления констант используется define(name, value)
        define('hello', "Hello, World");
        // для констант нет приставки $
        echo "<br>".hello;

        // для проверки существования константы используется функция bool defined(name)
        echo "<br>defined(hello): ".defined('hello');
        
        // Предопределенные константы: 
        // __FILE__, __LINE__, PHP_VERSION, PHP_OS, TRUE or true, FALSE or false
        echo "<br>__FILE__: ".__FILE__;
        echo "<br>__LINE__: ".__LINE__;
        echo "<br>PHP_VERSION: ".PHP_VERSION;
        echo "<br>PHP_OS: ".PHP_OS;
        echo "<br>true: ".true;
        echo "<br>false: ".false;        
    ?>
    <h2>Основные конструкции языка</h2>
    <h3>Операторы, операции и выражения</h3>
    <?php
        // 1. арифметические операции - как в С
        // 2. конкатенация строк через символ точка (.)
        // 3. операции сравнения - как в JavaScript (есть оператор эквивалентно === и неэквивалентно !==)
        // 4. условные конструкции как в С (if-elseif-else and switch)
        if(4>5) {
            
        } elseif(4<2) {
            
        } else {
        }
        // альтернативный синтаксис
        if(true):
            // code;
        elseif(false):
            //code
        else:
            //code
        endif;
        // switch тоже имеет альтернативный синтаксис
        
        switch ($a):
            case 1:
                //code
                break;
            case 2:
                //code
                break;
            default:
                break;
        endswitch;

        // Цикл while тоже имеет альтернативный синтаксис
        while(false):
            //code
        endwhile;
        
        $currentDate = time(); // текущая дата в секундах
        echo "<br>До пятници осталось:\n<ol>";
        while(date("l", $currentDate)!="Friday") // символ l а не цифра один
        {
            echo "<li>".date("l", $currentDate)."</li>\n";
            $currentDate+=60*60*24; // перемотать дату на сутки вперед
        }
        echo "</ol>\n";

        // цикл с постусловием do..while - как в С
        // цикл со счетчиком for and for..endfor
        // опция max_execution_time = 30 задает время выполнения сценария (30 сек по умолчанию) по истечении которого интерпертатор останавливает сценарий
        // break and continue работают как обычно в циклах        
    ?>
        
</body>

</html>