<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Lesson #2</title>
    </head>
    <body>
        <header>
            <h1>Урок №2</h1>
        </header>
        <article>
            <section>
                <header>
                    <h2>Массивы</h2>
                </header>
                <?php
                    // 1. Создание массива и присвоение ему значения
                    $my_array[0] = "value";
                    // Индексы могут идти не по порядку
                    $my_array[4] = "значение";
                    // Если использовать пустые квадратные скобки, то нумерация
                    // будет продолжаться
                    $my_array[] = 15; // индекс 5
                    $my_array[] = 26; // индекс 6
                    
                    // 2. Также массив может быть создан с помощью array()
                    $arr = array("first" => 1, 2 => "second", 3 => 3);
                    echo $arr["first"]; // 1
                    echo "<br>";
                    echo $arr[2]; // second
                    
                    // 3. Хорошим тоном считается создание пустого массива через
                    // array()
                    $narr = array();
                    $narr[] = "";
                    //...
                    
                    // 4. В ассоциативных массивах в качестве ключей используются
                    // строковые данные
                    // Ключ обязательно должен быть в кавычках если это не константа
                    $amenu["вино"] = "белое";
                    $amenu["пиво"] = "темное";
                    // В индексных массивах или списках в качестве ключей 
                    // используются числовые ключи
                    $lmenu[0] = "белое";
                    $lmenu[1] = "темное";
                    
                    // 5. Многомерные массивы
                    $marray = array(
                        "фрукты" => array("a"=>"апельсин",
                                          "b"=>"банан",
                                          "c"=>"яблоко"),
                        "числа" => array(1, 2, 3, 4, 5, 6),
                        "дырки" => array("первая", 5=>"вторая", "третья")
                    );
                    echo "<br />";
                    echo $marray["дырки"][0];
                    
                    // удалить элемент можно через функцию unset
                    unset($marray["числа"][3]);
                    
                    
                    echo "<br />";                    
                ?>
                <pre><?php
                    // для вывода многомерного массива может быть использована
                    // функция print_r()
                        print_r($marray);
                     ?>
                </pre>
                <?php
                    // Размер массива может быть вычислен при помощи функции
                    // count() что может быть использовано для перебора элементов
                    // списков
                    for($i=0; $i<count($lmenu); $i++) {
                        echo ($i+1)." ".$lmenu[$i]."<br/>";
                    }
                    
                    // Для перебора всех элементов м.б. использован цикл foreach
                    //foreach($amenu as $value) {
                    foreach($amenu as $key=>$value) {
                        echo "$key $value <br />";
                    }
                ?>
                <h2>Функции</h2>
                <?php
                    // 1. Синтаксис как в JavaScript
                    function foo() {
                        echo "Hello, function<br>";
                        return 0;
                    }                    
                    foo();
                    
                    // 2. По умолчанию значения передаются в функцию по значению
                    // что означает копирование данных
                    // Для передаче данных по ссылке необходимо указать &
                    function change($x) {
                        $x = 5;
                    }
                    $n = 10;
                    change($n);
                    echo "$n<br/>";
                    function change2(&$x) {
                        $x = 5;
                    }                    
                    change2($n);
                    echo "$n<br/>";
                    
                    // 3. Рекурсия
                    function fact($x) {
                        if($x<=1) {
                            return $x;
                        } else {
                            return $x * fact($x-1);
                        }                    
                    }
                    echo "5! = ".fact(5)."<br>";
                    
                    // 4. Область видимости переменных
                    // Существуют глобальные, локальные и статические переменные
                    // Глобальная переменная:
                    $x = 5;                    
                    function print_x() {
                        $l = 10; // локальная переменная видна только в функции
                        // внутри функции глобальная переменная не видна!!!
                        echo "x: ".@$x." <br/>"; // ошибка. добавление @ отключает сообщения об ошибке
                        global $x; // нужно указать, что переменная глобальная
                        echo "x: $x <br/>"; 
                        echo "x: ".$GLOBALS["x"]."<br/>"; // работает - предпочтительнее чем с global
                    }
                    print_x();
                    echo "l: ".@$l."<br>"; // ошибка
                    
                    // Статическая переменная - такая же, как и в С
                    function add() {
                        static $x = 0;
                        $x++;
                        echo "x: $x <br>";
                    }
                    add();
                    add();
                    add();
                    
                    // 5. Параметры по умолчанию - как в С++
                    function print_int($i=0) {
                        echo "i: $i<br>";
                    }
                    print_int(10);
                    print_int(0);
                    
                    // 6. Переменное число параметров
                    function myecho() {
                        // через массив for
                        for($i=0; $i<func_num_args(); $i++) {
                            echo $i.": ".func_get_arg($i)."<br/>";
                        }
                        // через foreach
                        foreach(func_get_args() as $arg) {
                            echo $arg."<br>";
                        }
                    }
                    myecho(1,2, "one", "two");
                    
                    // 7. Допустимы вложенные функции
                    function _parent($a) {
                        echo $a;
                        function child($b) {
                            // вложенная функция
                            echo $b+1;
                            return $b*$b;
                        }
                        return child($a)*$a*$a;
                    }
                    _parent(10);
                    child(30); // вложенные функции доступны в глобальном контексте
                    // обратный порядок вызова функций приведет к ошибке т.к.
                    // функция child не определена
                    // ошибка произойдет и в том случае, если повторно запустить
                    // функцию _parent
                    // _parent(2); ошибка
                    
                    // 8. Динамический вызов функий
                    // добавляет гибкость при работе с функциями
                    $myFunct = "myecho";
                    $myFunct(1,2,3);
                    $myFunct = "print_int";
                    $myFunct(345);
                ?>
                <h2>Библиотеки функций</h2>
                <?php
                    // 1. Подключение библиотеки
                    // 
                    // При возникновении ошибки - выдает предупреждение и 
                    // продолжает выполнение файла:
                    // include "path/to/file";
                    // 
                    // То же, но гарантирует однократное подключение файла:
                    // include_once "path/to/file"; 
                    // 
                    // При возникновении проблемы - останавливает выполнение
                    // require "path/to/file";
                    // 
                    // Аналогичная предыдущей:
                    // require_once "path/tp/file";
                    require_once 'my_math.lib.php';
                    echo math_sq(5);
                ?>
                <h2>Работа с формами</h2>
                <form action="#" method="get">
                    <label> Name:
                        <input type="text" name="name"> </label>
                    <input type="submit">
                </form>
                <?php
                    // существует суперглобальные перменные $_GET, $_POST, $_REQUEST
                    
                    // Получение форм через GET запрос:
                    //if(!@$_GET["name"]){
                    if(!filter_input(INPUT_GET, "name")){ // не будет ошибок                    
                        echo "Данные формы еще не заполнены<br>";
                    } else {
                        echo "Привет ".filter_input(INPUT_GET, "name")."<br>";
                    }
                    
                    // аналогично $_POST, $_REQUEST - содержит и get и post данные
                    
                    // массив $_SERVER
                    foreach($_SERVER as $key=>$val) {
                        echo "$key => $val <br>";
                    }
                ?>
            </section>
        </article>
    </body>
</html>
