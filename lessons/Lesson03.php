<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title></title>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="js/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/bootstrap.js" type="text/javascript"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header class="page-header">
            <h1>Lesson 03</h1>
            <nav class="dropdown">
                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                    Меню <span class="caret"></span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="#s1">Функции по работе с массивами</a>
                    </li>
                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="#s2">Математические функции</a>
                    </li>
                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="#s3">Функции для работы со строками</a>
                    </li>
                    <li role="presentation">
                        <a role="menuitem" tabindex="-1" href="#s4">Функции для работы с клаендарем</a>
                    </li>
                </ul>
            </nav>
        </header>
        .
        <article class="container">
            <section>
                <header>
                    <h2 id="s1">Функции по работе с массивами</h2>
                </header>
                <?php
                    // 1. Функция list
                    // Предположим, у нас есть массив, состоящий из трех элементов:
                    $names[0]="Александр";
                    $names[1]="Николай";
                    $names[2]="Яков";
                    // Допустим, в какой-то момент нам нужно передать значения 
                    // всех трех элементов массива, соответственно трем переменным: 
                    // $alex, $nick, $yakov. Это можно сделать так: 
                    $alex = $names[0];
                    $nick = $names[1];
                    $yakov = $names[2];
                    
                    // Есть более рациональный подход - использование функции list():
                    list ($alex, $nick, $yakov) = $names;
                    
                    
                ?>
            </section>
            <section>
                <header>
                    <h2 id="s2">Математические функции</h2>
                </header>
                <?php
                    //Математические константы
                    echo "<pre>";
                    echo "Pi: " . M_PI;
                    echo "e: " . M_E;
                    echo "pi/2: " . M_PI_2;
                    echo "pi/4: " . M_PI_4;
                    echo "sqrt(2)" . M_SQRT2;
                    echo "sqrt(3)" . M_SQRT3;
                    echo "</pre>";
                    // Перевод в различные системы счисления
                    echo "<pre>";
                    // из шестнадцатиричной в двоичную
                    echo base_convert("FF", 16, 2) . "\n";
                    // из двоичной в десятеричное
                    echo bindec("1111"). "\n";
                    // из десятеричное в двоичной
                    echo decbin(255). "\n";
                    echo "</pre>";
                ?>
            </section>
            <section>
                <header>
                    <h2 id="s3">Функции для работы со строками</h2>
                </header>
                <?php
                // put your code here
                echo "<pre>";
                echo htmlentities("<script>alert('test');</script>"). "\n";
                echo htmlspecialchars("<script>alert('test');</script>"). "\n";                
                echo "length: " . strlen('а') ."\n";
                echo "</pre>";
                ?>
            </section>
            <section>
                <header>
                    <h2 id="s4">Функции для работы с клаендарем</h2>
                </header>
                <?php
                // put your code here
                
                ?>
            </section>
        </article>
        <footer>

        </footer>
    </body>
</html>
