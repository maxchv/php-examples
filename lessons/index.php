<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Уроки PHP</title>
    </head>
    <body>
        <header>
            <h1>Уроки PHP</h1>
        </header>
        <article>
            <header>
                <h2></h2>
            </header>
            <p>PHP is acronym for "PHP: Hypertext Preprocessor"</p>
            <ul style='list-style-type: none;'>
            <?php
            
                for($i=1; $i<=5; $i++) {
                    $currDir = getcwd();
                    $baseDir = basename($currDir);
                    $filename = "lesson0".$i.".php";
                    if(file_exists($filename))
                    {
                        echo "<li><a href=lesson0".$i.".php>Урок ".$i."</a></li>";
                        echo "<ul><li>";
                        echo "<a href='../showContent.php?file=" . $baseDir. "/". $filename . "'>Код</a>";
                        echo "</li><li>";
                        echo "<a href='" . $filename . "'>Результат</a>";
                        echo "</li></ul>";
                    }
                }
            ?>
            </ul>            
        </article>
    </body>
</html>
