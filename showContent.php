<!doctype html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Example PHP</title>
        <link href="js/lib/codemirror-5.3/lib/codemirror.css" rel="stylesheet" type="text/css"/>
        <script src="js/lib/codemirror-5.3/lib/codemirror.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/mode/xml/xml.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/mode/javascript/javascript.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/mode/php/php.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/mode/css/css.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/mode/clike/clike.js" type="text/javascript"></script>
        <link href="js/lib/codemirror-5.3/theme/eclipse.css" rel="stylesheet" type="text/css"/>
        <link href="js/lib/codemirror-5.3/addon/hint/show-hint.css" rel="stylesheet" type="text/css"/>
        <script src="js/lib/codemirror-5.3/addon/hint/show-hint.js" type="text/javascript"></script>
        <link href="js/lib/codemirror-5.3/addon/fold/foldgutter.css" rel="stylesheet" type="text/css"/>
        <script src="js/lib/codemirror-5.3/addon/fold/foldcode.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/addon/fold/comment-fold.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/addon/fold/brace-fold.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/addon/fold/xml-fold.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/addon/fold/markdown-fold.js" type="text/javascript"></script>
        <script src="js/lib/codemirror-5.3/addon/fold/indent-fold.js" type="text/javascript"></script>
        <style>
            .CodeMirror {
                margin: 10px;
                border: 1px solid #eee;
                height: auto;                
            }
            #main {
                width: 70%;
                float: left;
                border: 1px solid black;
                padding: 0;
                margin: 0;
                font-size: 120%;
            }
            #out {
                position: fixed;
                display: block;                
                margin-top: 10px;
                margin-left: 10px;
                right: 10px;
                float: left;
                /*clear: right;*/
                height: auto;
                width: 40%;
                border: 1px solid black;
                padding: 0;
                margin: 0;
            }
            
            #back {
                right: 10px;
                position: fixed;
            }
        </style>
    </head>
    <body>        
        <form id="main"><textarea id="code" name="code"><?php
        header('Content-type: text/html; charset=utf-8');
        $filename = filter_input(INPUT_GET, 'file'); //$_REQUEST['file'];        
        if ($filename && is_file($filename)) {    
            $content = file_get_contents($filename);
            echo htmlspecialchars($content);            
        }        
        ?>
        </textarea></form>      
<!--        <div id="out"><?php eval( $content ); ?></div>-->
        <div id="back">
            <button onclick="history.back();">Назад</button>
        </div>
        <script>
            window.onload = function () {
                CodeMirror.commands.autocomplete = function (cm) {
                    CodeMirror.showHint(cm, CodeMirror.hint.auto);
                };
                window.editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                    mode: "text/x-php",
                    lineNumbers: true,
                    lineWrapping: true,
                    extraKeys: {"Ctrl-Space": "autocomplete"},
                    foldGutter: {
                        rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
                    },
                    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
                });
            };

        </script>
    </body>
</html>