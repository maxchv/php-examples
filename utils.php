<?php

function show()
{    
    echo '<pre>';
    $n = func_num_args();
    
    for($i = 0; $i < $n; $i++) {        
        $var = func_get_arg($i);
        if(isset($var)){
            $id = array_search($var, $GLOBALS);        
            echo "$$id = ";
            if(is_array($var)) {
                print_r($var);
            } else {
                var_dump($var);
            }
        }
    }
    echo '</pre>';
}
