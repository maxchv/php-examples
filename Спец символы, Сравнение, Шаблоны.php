﻿<?php

//—ѕ≈÷»јЋ№Ќџ≈ —»ћ¬ќЋџ 
//(сравнение с шаблоном)
//страница 117

empty()//пустой
isset()//установлено

//id
if(!empty($url_id[2]))
{
	$id = $url_id[2];
	
	if(!ereg("^[0-9]+$", $id)) 
	{
		header('Location: /404');
		exit();
	}
}

//проверка форм
if(isset($_POST['submit']))
{
	echo '<p><b id="o"> нопка нажата</b></p>';
}

//ошибки
if(!empty($ups))
{
	$ups = '<b id="u">'.$ups.'</b>';
	echo $ups;
}
//не ошибки
if(!empty($oke))
{
	$oke = '<b id="o">'.$oke.'</b>';
	echo $oke;
}

//фио
if(!empty($_POST['fio']))
{
	$fio = trim($_POST['fio']);
	
	if(!ereg("^[ј-яа-¤' ]+$", $fio)) 
	{
		$ups .= '¬водите им¤ отчество русскими буквами<br>';
	}
	elseif(strlen($fio) < 4) 
	{
		$ups .= '»м¤ отчество короче 4-х символов ('.strlen($fio).')<br>';
	}
	elseif(strlen($fio) > 255) 
	{
		$ups .= '»м¤ отчество длинее 255-ти символов ('.strlen($fio).')<br>';
	}
}
else{
	$ups .= '¬ведите им¤ отчество<br>';
}

//им¤
if(!empty($_POST['name']))
{
	$name = trim($_POST['name']);
	
	if(!ereg("^[ј-яа-¤]+$", $name)) 
	{
		$ups .= '¬водите им¤ русскими буквами<br>';
	}
	elseif(strlen($name) < 2) 
	{
		$ups .= '»м¤ короче 2-х символов ('.strlen($name).')<br>';
	}
	elseif(strlen($name) > 255) 
	{
		$ups .= '»м¤ длинее 255-ти символов ('.strlen($name).')<br>';
	}
}
else{
	$ups .= '¬ведите им¤<br>';
}

//логин
if(!empty($_POST['login']))
{
	$login = trim($_POST['login']);
	
	if(!ereg("^[A-za-z0-9]+$", $login)) 
	{
		$ups .= '¬водите логин английскими буквами или цифрами<br>';
	}
	elseif(strlen($login) < 2) 
	{
		$ups .= 'Ћогин короче 2-х символов ('.strlen($login).')<br>';
	}
	elseif(strlen($login) > 255) 
	{
		$ups .= 'Ћогин длинее 255-ти символов ('.strlen($login).')<br>';
	}
}
else{
	$ups .= '¬ведите логин<br>';
}
	

//емаил
if(!empty($_POST['email']))
{
	$email = strtolower(trim($_POST['email']));
	
	if(!preg_match("/^([a-z,0-9])+@([a-z,0-9])+(.([a-z,0-9])+)+$/", $email)) 
	{
		$ups .= 'Ќеверный формат e-mail<br>';
	}
	elseif(strlen($email) < 7) 
	{
		$ups .= 'E-mail короче 7-ми символов ('.strlen($email).')<br>';
	}
	elseif(strlen($email) > 50) 
	{
		$ups .= 'E-mail длинее 50-ти символов ('.strlen($email).')<br>';
	}
}
else{
	$ups .= '¬ведите e-mail<br>';
}
	
//телефон
if(!empty($_POST['tel']))
{
	$tel = trim($_POST['tel']);
	
	if(!ereg("^[0-9' + -]+$", $tel)) 
	{
		$ups .= '¬водите номер домашнего телефона только цифрами<br>';
	}
	elseif(strlen($tel) < 7) 
	{
		$ups .= 'Ќомер домашнего телефона короче 7-ми символов ('.strlen($tel).')<br>';
	}
	elseif(strlen($tel) > 15) 
	{
		$ups .= 'Ќомер домашнего телефона длинее 15-ти символов ('.strlen($tel).')<br>';
	}
}

//мобильный
if(!empty($_POST['telmob']))
{
	$telmob = trim($_POST['telmob']);
	
	if(!ereg("^[0-9' + -]+$", $telmob)) 
	{
		$ups .= '¬водите номер мобильного телефона только цифрами<br>';
	}
	elseif(strlen($telmob) < 7) 
	{
		$ups .= 'Ќомер мобильного телефона короче 7-ми символов ('.strlen($telmob).')<br>';
	}
	elseif(strlen($telmob) > 17) 
	{
		$ups .= 'Ќомер мобильного телефона длинее 17-ти символов ('.strlen($telmob).')<br>';
	}
}

//пароль
if(!empty($_POST['pass']))
{
	$pass = trim($_POST['pass']);
	$pass2 = trim($_POST['pass2']);
	
	if(!ereg("^[A-Za-z0-9'-]+$", $pass)) 
	{
		$ups .= '¬водите пароль английскими буквами и/или цифрами<br>';
	}
	elseif(strlen($pass) < 6) 
	{
		$ups .= 'ѕароль короче 6-ти символов<br>';
	}
	elseif(strlen($pass) > 17) 
	{
		$ups .= 'ѕароль длинее 17-ти символов<br>';
	}
}
else{
	$ups .= '¬ведите пароль<br>';
}

//пароль 2
if($_POST['pass'] != $_POST['pass2'])
{
	$ups .= 'ѕароли не совпадают<br>';
	$pass = '';
	$pass2 = '';
}

//антиспам
if(empty($_POST['robot']))
{
	$ups .= 'ѕоставьте птичку, что ¬ы не робот<br>';
}
else{
	$robot = 'checked="checked"';
}

//дл¤ проверки сайта
elseif(!preg_match("/^[a-z,0-9]+\.[a-z,0-9]+$/", $site))
elseif(!ereg("^[a-z,0-9.' -]+$", $site))

//ќѕ≈–ј÷»» —–ј¬Ќ≈Ќ»я
== равнозначны ли значени¤ двух переменных?
=== одинаковы ли как значени¤, так и типы двух переменных?
> больше ли первое значение, чем второе?
>= верно ли, что первое значение не меньше второго?
< меньше ли первое значение, чем второе?
<= верно ли,  что первое значение не больше второго?
!=,<> не равны ли значени¤ двух переменных?
!== не одинаковы ли значени¤ или типы данных двух переменных?

?>